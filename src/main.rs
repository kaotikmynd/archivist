mod commands;
mod archiver;

use std::ops::Sub;
use std::path::Path;
use std::process::exit;
use std::time::Instant;
use clap::{Parser};
use crate::archiver::{create_dir_if_not_exists, dir_is_valid, run_archive, run_summarize};
use crate::commands::{Cli, Commands};

fn main() {
    let cli = Cli::parse();
    let start = Instant::now();

    let count = match &cli.command {
        Commands::Archive(archive) => {
            let source_path = Path::new(&archive.source);
            let destination_path = Path::new(&archive.destination);
            let cores = match archive.cores {
                Some(count) => count,
                None => num_cpus::get()
            };

            if !dir_is_valid(source_path) {
                println!("Invalid source path: {}", source_path.display());
                exit(1)
            }

            match create_dir_if_not_exists(destination_path) {
                Err(e) => {
                    eprintln!("{}", e);
                    exit(1)
                },
                _ => ()
            }

            let retention_duration = chrono::Duration::days(i64::from(archive.days));
            let retention_date = chrono::Local::now().sub(retention_duration);

            match run_archive(&source_path, &destination_path, &retention_date, &cores) {
                Ok(file_count) => file_count,
                Err(error) => {
                    eprintln!("{}", error);
                    exit(1)
                }
            }
        },
        Commands::Summarize(summarize) => {
            let source_path = Path::new(&summarize.source);
            let cores = match summarize.cores {
                Some(count) => count,
                None => num_cpus::get()
            };

            if !dir_is_valid(source_path) {
                println!("Invalid source path: {}", source_path.display());
                exit(1)
            }

            let retention_duration = chrono::Duration::days(i64::from(summarize.days));
            let retention_date = chrono::Local::now().sub(retention_duration);

            match run_summarize(&source_path, &retention_date, &cores) {
                Ok(file_count) => file_count,
                Err(error) => {
                    eprintln!("{}", error);
                    exit(1)
                }
            }
        }
    };

    println!("Found {} file(s) in {}", count, humantime::Duration::from(start.elapsed()))
}
