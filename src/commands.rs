use clap::{Args, Parser, Subcommand};

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    Archive(Archive),
    Summarize(Summarize),
}

#[derive(Args)]
pub struct Archive {
    #[clap(short, long)]
    pub source: String,

    #[clap(short, long)]
    pub destination: String,

    #[clap(short, long)]
    pub days: u16,

    #[clap(short, long)]
    pub cores: Option<usize>,
}

#[derive(Args)]
pub struct Summarize {
    #[clap(short, long)]
    pub source: String,

    #[clap(short, long)]
    pub days: u16,

    #[clap(short, long)]
    pub cores: Option<usize>,
}