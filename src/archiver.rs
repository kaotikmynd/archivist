use std::borrow::Borrow;
use std::error::Error;
use std::fs;
use std::io::ErrorKind;
use std::path::{Path};
use std::sync::Arc;
use atomic_counter::{AtomicCounter, RelaxedCounter};
use chrono::prelude::*;
use jwalk::{Parallelism, WalkDir};
use crossbeam::channel::{Sender, unbounded};
use std::fs::copy;

pub fn dir_is_valid(path: &Path) -> bool {
    path.exists() && path.is_dir()
}

fn is_outside_retention_period(path: &Box<Path>, retention_date: &DateTime<Local>) -> Result<bool, Box<dyn Error>> {
    let metadata = match path.metadata() {
        Ok(meta) => meta,
        // if a file moves between when we scanned the directory and when we attempt
        // to check the modified time, just ignore it.
        Err(ref e) if e.kind() == ErrorKind::NotFound => return Ok(false),
        Err(e) => return Err(Box::new(e))
    };

    let mtime : &DateTime<Local> = &metadata.modified()?.into();

    return Ok(retention_date > mtime);
}

pub fn run_archive(source: &Path, destination: &Path, retention_date: &DateTime<Local>, cores: &usize) -> Result<i64, Box<dyn Error>> {
    let (s, r) = unbounded();

    // the sender is moved into the scan_files function
    // so when the function completes, the sender is dropped
    // and the channel is closed.
    scan_files(source, retention_date, s, cores)?;

    let pool = rayon::ThreadPoolBuilder::new().build()?;
    let counter = Arc::new(RelaxedCounter::new(0));

    pool.scope(|scope| {
        for _ in 0..*cores {
            let receiver = r.clone();
            let counter_clone = Arc::clone(&counter);

            scope.spawn(move |_| {
                // Keep accepting files from the channel until it is disconnected.
                loop {
                    let current_count = match receiver.recv() {
                        Ok(path) => {
                            archive_file(path.borrow(), source, destination);
                            counter_clone.inc() as i64
                        },
                        Err(_) => break
                    };

                    if current_count != 0 && current_count % 1000 == 0 {
                        println!("{}", current_count);
                    }
                }
            })
        }
    });

    return Ok(counter.get() as i64)
}

pub fn run_summarize(source: &Path, retention_date: &DateTime<Local>, cores: &usize) -> Result<i64, Box<dyn Error>> {
    let (s, r) = unbounded();

    // the sender is moved into the scan_files function
    // so when the function completes, the sender is dropped
    // and the channel is closed.
    scan_files(source, retention_date, s, cores)?;

    let pool = rayon::ThreadPoolBuilder::new().build()?;
    let counter = Arc::new(RelaxedCounter::new(0));

    pool.scope(|scope| {
        for _ in 0..*cores {
            let receiver = r.clone();
            let counter_clone = Arc::clone(&counter);

            scope.spawn(move |_| {
                loop {
                    let current_count = match receiver.recv() {
                        Ok(_) => { counter_clone.inc() as i64 },
                        // Keep accepting files from the channel until it is disconnected.
                        Err(_) => break
                    };

                    if current_count != 0 && current_count % 1000 == 0 {
                        println!("{}", current_count);
                    }
                }
            })
        }
    });

    return Ok(counter.get() as i64)
}

fn scan_files(source: &Path, retention_date: &DateTime<Local>, s: Sender<Box<Path>>, cores: &usize) -> Result<(), Box<dyn Error>> {
    for entry in WalkDir::new(source).parallelism(Parallelism::RayonNewPool(*cores)) {
        let path = entry?.path().into_boxed_path();

        if !path.is_dir() && is_outside_retention_period(&path, retention_date)? {
            s.send(path)?;
        }
    }

    return Ok(())
}

fn archive_file(file_path: &Path, source: &Path, destination: &Path) {
    let relative_path = match file_path.strip_prefix(source) {
        Ok(source) => source,
        Err(_) => panic!("Encountered a file that doesn't belong to the source directory.")
    };
    let new_file_path = destination.join(relative_path);

    match new_file_path.parent() {
        Some(dir) => {
            match create_dir_if_not_exists(dir) {
                Err(e) => println!("Unable to create {:?}: {}", dir, e),
                _ => ()
            }
        },
        None => panic!("Files should always have a parent dir")
    };

    match copy(file_path, &new_file_path) {
        Ok(_) => {
            match std::fs::remove_file(file_path) {
                Ok(_) => (),
                Err(e) => eprintln!("Failed to remove {}: {:?}", file_path.display(), e)
            }
        },
        Err(e) => eprintln!("Failed to copy {} to {}: {:?}", file_path.display(), new_file_path.display(), e)
    };
}

pub fn create_dir_if_not_exists(path: &Path) -> Result<(), Box<dyn Error>> {
    if !path.exists() {
        fs::create_dir_all(path)?
    }

    return Ok(())
}