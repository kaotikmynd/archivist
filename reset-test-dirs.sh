#!/usr/bin/env bash

testFileDirs="tests/test-dirs"

testDir1="$testFileDirs/test-dir1"
testDir2="$testFileDirs/test-dir2"

if [ -d "$testDir1" ]; then
  rm -rf "$testDir1"
fi

if [ -d "$testDir2" ]; then
  rm -rf "$testDir2"
fi

mkdir -p "$testDir2"

#sudo chown root:root "$testDir2"

mkdir -p $testDir1/{dir1,dir2,dir3}
mkdir -p $testDir1/dir2/{dir4,dir5}

touch -d "2021-01-01T00:00:00" "$testDir1/dir2/file1.txt"
touch "$testDir1/dir2/dir5/file2.txt"
touch -d "2021-01-01T00:00:00" "$testDir1/dir3/file3.txt"

tree "$testDir1"